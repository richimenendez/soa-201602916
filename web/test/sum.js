/* test/sum.js */

var sum = require('../index.js');
var expect = require('chai').expect;

describe('#sum()', function() {

  
  context('Suma con argumentos', function() {
    it('Debería retornar la suma de los argumentos', function() {
      expect(sum(1, 2)).to.equal(4)
    })
  })
  
})